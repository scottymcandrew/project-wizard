terraform {
  backend "s3" {
    bucket = "sdm-eks-cluster-tfstate"
    key    = "sdm-eks-cluster-tfstate-state"
    region = "us-east-2"
  }
}