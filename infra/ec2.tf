resource "aws_iam_instance_profile" "instance_profile_mongo_host" {
  name = "instance_profile_mongo_host"
  role = aws_iam_role.role_mongo_host.name
}

resource "aws_iam_role" "role_mongo_host" {
  name = "role_mongo_host"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "policy_mongo_host" {
  name = "policy_mongo_host"
  role = aws_iam_role.role_mongo_host.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:*",
        ]
        Effect = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "s3:*",
        ]
        Effect = "Allow"
        Resource = "*"
      },
    ]
  })
}

module "ec2_instance" {
  source = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "mongodb-instance"

  ami = "ami-0a2a650e797f8ca97"
  instance_type = "t2.micro"
  key_name = "sdm_keypair_ohio"
  monitoring = true
  vpc_security_group_ids = [
    aws_security_group.mongodb-ec2.id]
  subnet_id = module.vpc.private_subnets[0]
  iam_instance_profile = "instance_profile_mongo_host"

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}