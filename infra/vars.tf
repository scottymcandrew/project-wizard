variable "MONGODB_PASSWORD" {
  type = string
}

variable "MONGODB_USERNAME" {
  type = string
}